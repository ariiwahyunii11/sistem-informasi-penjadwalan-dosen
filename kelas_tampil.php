<h1 align="center">DATA KELAS</h1>

<a class="btn btn-primary" href="?page=kelas&action=tambah">Add Data</a>
<table class="table table-bordered" id="myTables">
    <thead>
      <tr>
  
        <th width="250px">Nama Kelas</th>
        <th width="300px">Program Studi</th>
        <th width="300px">Fakultas</th>
        <th width="200px">Opsi</th>
      </tr>
    </thead>
    <tbody>
       <!-- awal proses menampilkan data -->
    <?php
     $sql = "SELECT*FROM kelas ORDER BY id_kelas ASC";
     $result = $conn->query($sql);
     while($row = $result->fetch_assoc()) {
    ?>
    <tr>
  
  <td><?php echo $row['nama_kelas'];?></td>
	<td><?php echo $row['prodi'];?></td>
	<td><?php echo $row['fakultas'];?></td>
	<td align="center">
        <a class="btn btn-info" href="?page=kelas&action=update&id_kelas=<?php echo $row['id_kelas']; ?>">Edit</a>
        <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=kelas&action=hapus&id_kelas=<?php echo $row['id_kelas']; ?>">Delete</a>
    </td>
    </tr>
    <?php
     }
     $conn->close();
    ?>
   <!-- akhir proses menampilkan data -->
    
   </tbody>
</table>