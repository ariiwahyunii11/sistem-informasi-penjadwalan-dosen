<h1 align="center">DATA JADWAL KELAS</h1>

<a class="btn btn-primary" href="?page=jadwalkelas&action=tambah" style="margin-bottom: 10px;">Add Data</a>
<table class="table table-bordered" id="myTables" >
    <thead>
      <tr>
        <th>Jadwal</th>
        <th>Mata Kuliah</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
	<!-- letakkan proses menampilkan disini -->
    <?php
     $sql = "SELECT*FROM jadwalkelas ORDER BY id_jadwal ASC";
     $result = $conn->query($sql);
     while($row = $result->fetch_assoc()) {
    ?>
     <tr>
    <td><?php echo $row['jadwal']; ?></td>
	<td><?php echo $row['mata_kuliah']; ?></td>

	<td align="center">
            <a class="btn btn-info" href="?page=jadwalkelas&action=update&id_jadwal=<?php echo $row['id_jadwal']; ?>">Edit</a>
            <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=jadwalkelas&action=hapus&id_jadwal=<?php echo $row['id_jadwal']; ?>">Hapus</a>
        </td>
     </tr>
    <?php
     }
     $conn->close();
 ?>
   </tbody>
</table>