<!-- awal proses update -->
<?php 

//proses update
if(isset($_POST['update'])){
    $id_dosen=$_POST['id_dosen'];
    $foto_dosen=$_POST['foto_dosen'];
    $nip_dosen=$_POST['nip_dosen'];
    $nama_dosen=$_POST['nama_dosen'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];

    // proses update
    $sql = "UPDATE dosen SET foto_dosen='$foto_dosen',nip_dosen='$nip_dosen',nama_dosen='$nama_dosen',prodi='$prodi',fakultas='$fakultas' WHERE id_dosen='$id_dosen'";
    if ($conn->query($sql) === TRUE) {
        header("Location:?page=dosen");
    }
}

//menampilkan data di form
$id_dosen=$_GET['id_dosen'];

$sql = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!-- akhir proses update -->

<form action="" method="POST">
    <div class="row">
        <div class="col-sm-6">
        <h1 align="center">Edit Data Dosen</h1>

            <div class="form-group">
            <label for="id_dosen">Id Dosen</label>
            <input type="text" class="form-control" value="<?php echo $row['id_dosen']; ?>" name="id_dosen" readonly><br>
            </div>

            <div class="form-group">
            <label for="foto_dosen">Foto Dosen</label>
            <input type="file" class="form-control" value="<?php echo $row['foto_dosen']; ?>" name="foto_dosen"  required><br>
            </div>

            <div class="form-group">
            <label for="nip_dosen"> NIP Dosen</label>
            <input type="text" class="form-control" value="<?php echo $row['nip_dosen']; ?>" name="nip_dosen" maxlength="25" required><br>
            </div>

            <div class="form-group">
            <label for="nama_dosen">Nama Dosen</label>
            <input type="text" class="form-control" value="<?php echo $row['nama_dosen']; ?>" name="nama_dosen" maxlength="255" required><br>
            </div>

            <div class="form-group">
            <label for="prodi">Program Studi</label>
            <input type="text" class="form-control" value="<?php echo $row['prodi']; ?>" name="prodi" maxlength="255" required><br>
            </div>

            <div class="form-group">
            <label for="fakultas">Fakultas</label>
            <input type="text" class="form-control" value="<?php echo $row['fakultas']; ?>" name="fakultas" maxlength="255" required><br>
            </div> 
            <input class="btn btn-primary" type="submit" name="update" value="Update">
            <a class="btn btn-danger" href="?page=dosen">Cancel</a>
        </div>
    </div>
</form>

<?php
$conn->close();
?>