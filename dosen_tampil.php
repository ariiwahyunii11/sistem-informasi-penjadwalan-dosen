<h1 align="center">DATA DOSEN</h1>

<a class="btn btn-primary" href="?page=dosen&action=tambah">Add Data</a>
<table class="table table-bordered" id="myTables">
    <thead>
      <tr>
        <th>Foto Dosen</th>
        <th>NIP Dosen</th>
        <th>Nama Dosen</th>
        <th>Program Studi</th>
        <th>Fakultas</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
	<!-- letakkan proses menampilkan disini -->

    <?php
     $sql = "SELECT*FROM dosen ORDER BY id_dosen ASC";
     $result = $conn->query($sql);
     while($row = $result->fetch_assoc()) {
    ?>
    <tr>
    <td><?php echo $row['foto_dosen']; ?></td>
	  <td><?php echo $row['nip_dosen']; ?></td>
	  <td><?php echo $row['nama_dosen']; ?></td>
    <td><?php echo $row['prodi']; ?></td>
    <td><?php echo $row['fakultas']; ?></td>

	<td align="center">
      <a class="btn btn-info" href="?page=dosen&action=update&id_dosen=<?php echo $row['id_dosen']; ?>">Edit</a>
      <a onclick="return confirm('Yakin menghapus data ini ?')" class="btn btn-danger" href="?page=dosen&action=hapus&id_dosen=<?php echo $row['id_dosen']; ?>">Delete</a>
  </td>
    </tr>
    <?php
     }
     $conn->close();
    ?>
   </tbody>
</table>