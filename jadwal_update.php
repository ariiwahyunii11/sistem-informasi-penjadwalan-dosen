<!-- awal proses update -->
<?php 

//proses update
if(isset($_POST['update'])){
    $id_jadwal=$_POST['id_jadwal'];
    $id_dosen=$_POST['id_dosen'];
    $id_kelas=$_POST['id_kelas'];
    $jadwal=$_POST['jadwal'];
    $mata_kuliah=$_POST['mata_kuliah'];
  
    // proses update
    $sql = "UPDATE jadwalkelas SET jadwal='$jadwal',mata_kuliah='$mata_kuliah' WHERE id_jadwal='$id_jadwal'";
    if ($conn->query($sql) === TRUE) {
        header("Location:?page=jadwalkelas");
    }
}

//menampilkan data di form
$id_jadwal=$_GET['id_jadwal'];

$sql = "SELECT * FROM jadwalkelas WHERE id_jadwal='$id_jadwal'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!-- akhir proses update -->

<form action="" method="POST">
    <div class="row">
        <div class="col-sm-6">
        <h1 align="center">Edit Data Jadwal Kelas</h1>

            <div class="form-group">
            <label for="id_jadwal">Id Jadwal</label>
            <input type="text" class="form-control" value="<?php echo $row['id_jadwal']; ?>" name="id_jadwal" readonly><br>
            </div>

            <div class="form-group">
            <label for="id_dosen">Id Dosen</label>
            <input type="text" class="form-control" value="<?php echo $row['id_dosen']; ?>" name="id_dosen" readonly><br>
            </div>

            <div class="form-group">
            <label for="id_kelas">Id Kelas</label>
            <input type="text" class="form-control" value="<?php echo $row['id_kelas']; ?>" name="id_kelas" readonly><br>
            </div>

            <div class="form-group">
            <label for="jadwal">Jadwal</label>
            <input type="text" class="form-control" value="<?php echo $row['jadwal']; ?>" name="jadwal" maxlength="255" required><br>
            </div> 

            <div class="form-group">
            <label for="mata_kuliah">Mata Kuliah</label>
            <input type="text" class="form-control" value="<?php echo $row['mata_kuliah']; ?>" name="mata_kuliah" maxlength="255" required><br>
            </div> 
            <input class="btn btn-primary" type="submit" name="update" value="Update">
            <a class="btn btn-danger" href="?page=jadwalkelas">Cancel</a>
        </div>
    </div>
</form>

<?php
$conn->close();
?>