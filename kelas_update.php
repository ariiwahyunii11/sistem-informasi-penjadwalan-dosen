<!-- awal proses update -->
<?php 

//proses update
if(isset($_POST['update'])){
    $id_kelas=$_POST['id_kelas'];
    $nama_kelas=$_POST['nama_kelas'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];
  
    // proses update
    $sql = "UPDATE kelas SET nama_kelas='$nama_kelas',prodi='$prodi',fakultas='$fakultas' WHERE id_kelas='$id_kelas'";
    if ($conn->query($sql) === TRUE) {
        header("Location:?page=kelas");
    }
}

//menampilkan data di form
$id_kelas=$_GET['id_kelas'];

$sql = "SELECT * FROM kelas WHERE id_kelas='$id_kelas'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
?>
<!-- akhir proses update -->

<form action="" method="POST">
    <div class="row">
        <div class="col-sm-6">
        <h1 align="center">Edit Data Kelas</h1>

            <div class="form-group">
            <label for="id_kelas">Id Kelas</label>
            <input type="text" class="form-control" value="<?php echo $row['id_kelas']; ?>" name="id_kelas" readonly><br>
            </div>

            <div class="form-group">
            <label for="nama_kelas">Nama Kelas</label>
            <input type="text" class="form-control" value="<?php echo $row['nama_kelas']; ?>" name="nama_kelas" maxlength="255" required><br>
            </div>

            <div class="form-group">
            <label for="prodi">Program Studi</label>
            <input type="text" class="form-control" value="<?php echo $row['prodi']; ?>" name="prodi" maxlength="255" required><br>
            </div>

            <div class="form-group">
            <label for="fakultas">Fakultas</label>
            <input type="text" class="form-control" value="<?php echo $row['fakultas']; ?>" name="fakultas" maxlength="255" required><br>
            </div> 
            <input class="btn btn-primary" type="submit" name="update" value="Update">
            <a class="btn btn-danger" href="?page=kelas">Cancel</a>
        </div>
    </div>
</form>

<?php
$conn->close();
?>