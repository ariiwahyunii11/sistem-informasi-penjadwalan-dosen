<!-- awal proses simpan-->
<?php

if(isset($_POST['simpan'])){

    //mengambil data dari form
    $id_dosen=$_POST['id_dosen'];
    $foto_dosen=$_POST['foto_dosen'];
    $nip_dosen=$_POST['nip_dosen'];
    $nama_dosen=$_POST['nama_dosen'];
    $prodi=$_POST['prodi'];
    $fakultas=$_POST['fakultas'];
	
    // validasi nip
    $sql = "SELECT*FROM dosen WHERE nip_dosen='$nip_dosen'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        ?>
            <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>NIP sudah digunakan. Inputkan NIP baru! </strong> <br>
            </div>
        <?php
    }else{
	//proses simpan
        $sql = "INSERT INTO dosen VALUES ('$id_dosen','$foto_dosen','$nip_dosen','$nama_dosen','$prodi','$fakultas')";
        if ($conn->query($sql) === TRUE) {
            header("Location:?page=dosen");
        }
    }
    $conn->close();
}
?>
<!-- akhir proses simpan-->

<form action="" method="POST">
    <div class="row">
        <div class="col-sm-6">

        <h1 align="center">Input Data Dosen</h1>

            <div class="form-group">
            <label for="">Id Dosen</label>
            <input type="text" class="form-control" name="id_dosen" maxlength="25"required>
            </div>

            <div class="form-group">
            <label for="">Foto Dosen</label>
            <input type="file" class="form-control" name="foto_dosen" required>
            </div>

            <div class="form-group">
            <label for="">NIP Dosen</label>
            <input type="text" class="form-control" name="nip_dosen" maxlength="25" required>
            </div>

            <div class="form-group">
            <label for="">Nama Dosen</label>
            <input type="text" class="form-control" name="nama_dosen" maxlength="255" required>
            </div>
           
            <div class="form-group">
            <label for="">Program Studi</label>
            <input type="text" class="form-control" name="prodi" maxlength="255" required>
            </div>

            <div class="form-group">
            <label for="">Fakultas</label>
            <input type="text" class="form-control" name="fakultas" maxlength="255" required>
            </div>

            <input class="btn btn-success" type="submit" name="simpan" value="Save">
            <a class="btn btn-danger" href="?page=dosen">Cancel</a>
        </div>
    </div>
</form>